	.file	"test.c"
	.text
	.globl	writeint
	.type	writeint, @function
writeint:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$88, %esp
	movl	%gs:20, %eax
	movl	%eax, -12(%ebp)
	xorl	%eax, %eax
	movl	$2608, -32(%ebp)
	movl	$0, -28(%ebp)
	movl	$0, -24(%ebp)
	movl	$0, -20(%ebp)
	movl	$0, -16(%ebp)
	leal	-52(%ebp), %eax
	movl	%eax, -60(%ebp)
	leal	-32(%ebp), %eax
	movl	%eax, -56(%ebp)
	cmpl	$0, 8(%ebp)
	jns	.L2
	movl	-56(%ebp), %eax
	movb	$45, (%eax)
	addl	$1, -56(%ebp)
	negl	8(%ebp)
.L2:
	cmpl	$0, 8(%ebp)
	jle	.L3
	jmp	.L4
.L5:
	movl	8(%ebp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	addl	$48, %eax
	movl	%eax, %edx
	movl	-60(%ebp), %eax
	movb	%dl, (%eax)
	addl	$1, -60(%ebp)
	movl	8(%ebp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, 8(%ebp)
.L4:
	cmpl	$0, 8(%ebp)
	jg	.L5
	subl	$1, -60(%ebp)
	jmp	.L6
.L7:
	movl	-60(%ebp), %eax
	movzbl	(%eax), %edx
	movl	-56(%ebp), %eax
	movb	%dl, (%eax)
	addl	$1, -56(%ebp)
	subl	$1, -60(%ebp)
.L6:
	leal	-52(%ebp), %eax
	cmpl	%eax, -60(%ebp)
	ja	.L7
	movl	-60(%ebp), %eax
	movzbl	(%eax), %edx
	movl	-56(%ebp), %eax
	movb	%dl, (%eax)
	addl	$1, -56(%ebp)
	movl	-56(%ebp), %eax
	movb	$10, (%eax)
	addl	$1, -56(%ebp)
	movl	-56(%ebp), %eax
	movb	$0, (%eax)
	addl	$1, -56(%ebp)
	jmp	.L8
.L3:
	leal	-32(%ebp), %eax
	addl	$3, %eax
	movl	%eax, -56(%ebp)
.L8:
	movl	-56(%ebp), %edx
	leal	-32(%ebp), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	subl	$1, %eax
	movl	%eax, 8(%esp)
	leal	-32(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	write
	movl	-12(%ebp), %eax
	xorl	%gs:20, %eax
	je	.L9
	call	__stack_chk_fail
.L9:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	writeint, .-writeint
	.globl	readint
	.type	readint, @function
readint:
.LFB1:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$104, %esp
	movl	%gs:20, %eax
	movl	%eax, -12(%ebp)
	xorl	%eax, %eax
	movl	$2608, -32(%ebp)
	movl	$0, -28(%ebp)
	movl	$0, -24(%ebp)
	movl	$0, -20(%ebp)
	movl	$0, -16(%ebp)
	leal	-52(%ebp), %eax
	movl	%eax, -76(%ebp)
	leal	-32(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	$0, -72(%ebp)
	movl	$20, 8(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$0, (%esp)
	call	read
	movl	%eax, -56(%ebp)
	subl	$1, -56(%ebp)
	movl	$0, -68(%ebp)
	leal	-52(%ebp), %eax
	movl	%eax, -76(%ebp)
	jmp	.L11
.L12:
	addl	$1, -76(%ebp)
	addl	$1, -68(%ebp)
.L11:
	movl	-56(%ebp), %eax
	subl	$1, %eax
	cmpl	-68(%ebp), %eax
	jg	.L12
	movl	-56(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	$1, -64(%ebp)
	jmp	.L13
.L14:
	movl	-76(%ebp), %eax
	movzbl	(%eax), %eax
	movsbl	%al, %eax
	subl	$48, %eax
	imull	-64(%ebp), %eax
	addl	%eax, -72(%ebp)
	subl	$1, -76(%ebp)
	subl	$1, -68(%ebp)
	movl	-64(%ebp), %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, -64(%ebp)
.L13:
	cmpl	$0, -68(%ebp)
	jg	.L14
	movl	-72(%ebp), %eax
	movl	-12(%ebp), %edx
	xorl	%gs:20, %edx
	je	.L15
	call	__stack_chk_fail
.L15:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE1:
	.size	readint, .-readint
	.globl	multiply
	.type	multiply, @function
multiply:
.LFB2:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	8(%ebp), %eax
	imull	12(%ebp), %eax
	popl	%ebp
	.cfi_def_cfa 4, 4
	.cfi_restore 5
	ret
	.cfi_endproc
.LFE2:
	.size	multiply, .-multiply
	.globl	divide
	.type	divide, @function
divide:
.LFB3:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	8(%ebp), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	idivl	12(%ebp)
	popl	%ebp
	.cfi_def_cfa 4, 4
	.cfi_restore 5
	ret
	.cfi_endproc
.LFE3:
	.size	divide, .-divide
	.globl	main
	.type	main, @function
main:
.LFB4:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$16, %esp
	movl	$3, 4(%esp)
	movl	$5, (%esp)
	call	multiply
	movl	%eax, (%esp)
	call	writeint
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE4:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
