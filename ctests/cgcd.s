STDOUT = 1
STDIN = 0
.data
.comm _Cgcd_x, 4, 4
.comm _Cgcd_y, 4, 4
.comm _Cgcd_ans, 4, 4
.text
_Cgcd_gcd:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl 12(%ebp)
	pushl $0
	popl %eax
	popl %ebx
	cmpl %eax, %ebx
	jne _Feq1
_Teq1:
	pushl $1
	jmp _Eeq1
_Feq1:
	pushl $0
_Eeq1:
	popl %eax
	cmpl $0, %eax
	jne _if1
	jmp _else1
_if1:
	pushl 8(%ebp)
	popl %eax
	movl %eax, -4(%ebp)
	jmp _endif1
_else1:
	pushl 8(%ebp)
	pushl 8(%ebp)
	pushl 12(%ebp)
	call divide
	addl $8, %esp
	pushl %eax
	pushl 12(%ebp)
	call multiply
	addl $8, %esp
	pushl %eax
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	pushl 12(%ebp)
	call _Cgcd_gcd
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, 8(%ebp)
	pushl 8(%ebp)
	popl %eax
	movl %eax, -4(%ebp)
	jmp _endif1
_endif1:
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_Cgcd_displayres:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl 8(%ebp)
	call writeint
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_Cgcd_start:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	call readint
	pushl %eax
	popl %eax
	movl %eax, _Cgcd_x
	call readint
	pushl %eax
	popl %eax
	movl %eax, _Cgcd_y
	pushl _Cgcd_y
	pushl _Cgcd_x
	call _Cgcd_gcd
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, _Cgcd_ans
	pushl _Cgcd_ans
	call _Cgcd_displayres
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
.global main
main:
	call _Cgcd_start
	pushl $0
	call exit
