STDOUT = 1
STDIN = 0
.data
.comm _CFact_num, 4, 4
.comm _CFact_num2, 4, 4
.text
_CFact_Fact:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl $0
	pushl 8(%ebp)
	pushl $0
	popl %eax
	popl %ebx
	cmpl %eax, %ebx
	jne _Feq1
_Teq1:
	pushl $1
	jmp _Eeq1
_Feq1:
	pushl $0
_Eeq1:
	popl %eax
	cmpl $0, %eax
	jne _if1
	jmp _else1
_if1:
	pushl $1
	popl %eax
	movl %eax, -8(%ebp)
	jmp _endif1
_else1:
	pushl 8(%ebp)
	pushl 8(%ebp)
	pushl $1
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	call _CFact_Fact
	addl $4, %esp
	pushl %eax
	call multiply
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, -8(%ebp)
	jmp _endif1
_endif1:
	pushl -8(%ebp)
	popl %eax
	movl %eax, -4(%ebp)
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CFact_Go:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl $0
	pushl $0
	popl %eax
	movl %eax, -8(%ebp)
_while1:
	pushl -8(%ebp)
	call notExpr
	addl $4, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _startwhile1
	jmp _endwhile1
_startwhile1:
	call readint
	pushl %eax
	popl %eax
	movl %eax, _CFact_num
	pushl _CFact_num
	pushl $1
	call gtequalto
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, -8(%ebp)
	jmp _while1
_endwhile1:
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CFact_start:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	call _CFact_Go
	addl $0, %esp
	pushl _CFact_num
	call _CFact_Fact
	addl $4, %esp
	pushl %eax
	popl %eax
	movl %eax, _CFact_num2
	pushl _CFact_num
	call writeint
	addl $4, %esp
	pushl _CFact_num2
	call writeint
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
.global main
main:
	call _CFact_start
	pushl $0
	call exit
