STDOUT = 1
STDIN = 0
.data
.comm _CIterfact_num, 4, 4
.comm _CIterfact_num2, 4, 4
.comm _CIterfact_isOk, 4, 4
.text
_CIterfact_Fact:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl $0
	pushl $1
	popl %eax
	movl %eax, -8(%ebp)
_while1:
	pushl 8(%ebp)
	pushl $0
	call greater
	addl $8, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _startwhile1
	jmp _endwhile1
_startwhile1:
	pushl -8(%ebp)
	pushl 8(%ebp)
	call multiply
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, -8(%ebp)
	pushl 8(%ebp)
	pushl $1
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	popl %eax
	movl %eax, 8(%ebp)
	jmp _while1
_endwhile1:
	pushl -8(%ebp)
	popl %eax
	movl %eax, -4(%ebp)
	pushl $0
	pushl -8(%ebp)
	popl %eax
	popl %ebx
	addl %ebx, %eax
	pushl %eax
	pushl -8(%ebp)
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	call writeint
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CIterfact_start:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	call readint
	pushl %eax
	popl %eax
	movl %eax, _CIterfact_num
	pushl _CIterfact_num
	pushl $0
	call greater
	addl $8, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _if1
	jmp _else1
_if1:
	pushl _CIterfact_num
	call _CIterfact_Fact
	addl $4, %esp
	pushl %eax
	popl %eax
	movl %eax, _CIterfact_num
	pushl _CIterfact_num
	call writeint
	addl $4, %esp
	jmp _endif1
_else1:
	jmp _endif1
_endif1:
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
.global main
main:
	call _CIterfact_start
	pushl $0
	call exit
