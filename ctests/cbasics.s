STDOUT = 1
STDIN = 0
.data
.comm _CBasics_d, 4, 4
.text
_CBasics_meth2:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl $0
	pushl 8(%ebp)
	pushl 12(%ebp)
	popl %eax
	popl %ebx
	addl %ebx, %eax
	pushl %eax
	popl %eax
	movl %eax, -4(%ebp)
	pushl $10
	popl %eax
	movl %eax, 8(%ebp)
	pushl 8(%ebp)
	call writeint
	addl $4, %esp
	pushl 12(%ebp)
	call writeint
	addl $4, %esp
	pushl -8(%ebp)
	call writeint
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CBasics_meth1:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl $0
	pushl $-5
	popl %eax
	movl %eax, _CBasics_d
	pushl _CBasics_d
	pushl 8(%ebp)
	pushl 12(%ebp)
	popl %eax
	popl %ebx
	addl %ebx, %eax
	pushl %eax
	call _CBasics_meth2
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, -4(%ebp)
	pushl 8(%ebp)
	call writeint
	addl $4, %esp
	pushl 12(%ebp)
	call writeint
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CBasics_start:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl $0
	pushl $0
	pushl $0
	pushl $10
	popl %eax
	movl %eax, -12(%ebp)
	pushl $20
	popl %eax
	movl %eax, -16(%ebp)
	pushl -8(%ebp)
	call writeint
	addl $4, %esp
	pushl -12(%ebp)
	call writeint
	addl $4, %esp
	pushl -16(%ebp)
	call writeint
	addl $4, %esp
	pushl _CBasics_d
	call writeint
	addl $4, %esp
	pushl -16(%ebp)
	pushl -12(%ebp)
	call _CBasics_meth1
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, -8(%ebp)
	pushl _CBasics_d
	call writeint
	addl $4, %esp
	pushl -8(%ebp)
	call writeint
	addl $4, %esp
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
.global main
main:
	call _CBasics_start
	pushl $0
	call exit
