STDOUT = 1
STDIN = 0
.data
.comm _CChange_Amt, 4, 4
.comm _CChange_Quarters, 4, 4
.comm _CChange_Dimes, 4, 4
.comm _CChange_Nickels, 4, 4
.text
_CChange_ComputeChange:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl 8(%ebp)
	pushl 12(%ebp)
	call divide
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, -4(%ebp)
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CChange_ComputeRemain:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	pushl 8(%ebp)
	pushl 12(%ebp)
	pushl 16(%ebp)
	call multiply
	addl $8, %esp
	pushl %eax
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	popl %eax
	movl %eax, -4(%ebp)
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
_CChange_start:
	pushl %ebp
	movl %esp, %ebp
	pushl $0
	call readint
	pushl %eax
	popl %eax
	movl %eax, _CChange_Amt
	pushl $25
	pushl _CChange_Amt
	call _CChange_ComputeChange
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, _CChange_Quarters
	pushl _CChange_Quarters
	pushl $0
	call greater
	addl $8, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _if1
	jmp _else1
_if1:
	pushl _CChange_Quarters
	call writeint
	addl $4, %esp
	jmp _endif1
_else1:
	pushl $0
	call writeint
	addl $4, %esp
	jmp _endif1
_endif1:
	pushl _CChange_Quarters
	pushl $25
	pushl _CChange_Amt
	call _CChange_ComputeRemain
	addl $12, %esp
	pushl %eax
	popl %eax
	movl %eax, _CChange_Amt
	pushl $10
	pushl _CChange_Amt
	call _CChange_ComputeChange
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, _CChange_Dimes
	pushl _CChange_Dimes
	pushl $0
	call greater
	addl $8, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _if2
	jmp _else2
_if2:
	pushl _CChange_Dimes
	call writeint
	addl $4, %esp
	jmp _endif2
_else2:
	pushl $0
	call writeint
	addl $4, %esp
	jmp _endif2
_endif2:
	pushl _CChange_Dimes
	pushl $10
	pushl _CChange_Amt
	call _CChange_ComputeRemain
	addl $12, %esp
	pushl %eax
	popl %eax
	movl %eax, _CChange_Amt
	pushl $5
	pushl _CChange_Amt
	call _CChange_ComputeChange
	addl $8, %esp
	pushl %eax
	popl %eax
	movl %eax, _CChange_Nickels
	pushl _CChange_Nickels
	pushl $0
	call greater
	addl $8, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _if3
	jmp _else3
_if3:
	pushl _CChange_Nickels
	call writeint
	addl $4, %esp
	jmp _endif3
_else3:
	pushl $0
	call writeint
	addl $4, %esp
	jmp _endif3
_endif3:
	pushl _CChange_Nickels
	pushl $5
	pushl _CChange_Amt
	call _CChange_ComputeRemain
	addl $12, %esp
	pushl %eax
	popl %eax
	movl %eax, _CChange_Amt
	pushl _CChange_Amt
	pushl $0
	call greater
	addl $8, %esp
	pushl %eax
	popl %eax
	cmpl $0, %eax
	jne _if4
	jmp _else4
_if4:
	pushl _CChange_Amt
	call writeint
	addl $4, %esp
	jmp _endif4
_else4:
	jmp _endif4
_endif4:
	movl -4(%ebp), %eax
	movl %ebp, %esp
	popl %ebp
	ret
.text
.global main
main:
	call _CChange_start
	pushl $0
	call exit
