STDOUT = 1
STDIN = 0
.data
.comm _Assign1_x, 4, 4
.comm _Assign1_y, 4, 4
.comm _Assign1_z, 4, 4
.comm _Assign1_b1, 4, 4
.comm _Assign1_b2, 4, 4
.text
_Assign1_start:
.data
.text
	pushl $5
	popl _Assign1_x
	pushl _Assign1_x
	pushl $8
	pushl _Assign1_x
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	call multiply
	addl $8, %esp
	pushl %eax
	popl _Assign1_y
	pushl $1
	popl _Assign1_b1
	pushl $0
	call notExpr
	addl $4, %esp
	pushl %eax
	popl _Assign1_b2
	pushl _Assign1_b2
	call notExpr
	addl $4, %esp
	pushl %eax
	pushl _Assign1_b1
	call binaryOr
	addl $8, %esp
	pushl %eax
	call notExpr
	addl $4, %esp
	pushl %eax
	popl _Assign1_b2
	pushl _Assign1_y
	call writeint
	addl $4, %esp
	pushl _Assign1_x
	pushl $2
	call divide
	addl $8, %esp
	pushl %eax
	call writeint
	addl $4, %esp
	pushl _Assign1_y
	pushl _Assign1_x
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	popl _Assign1_y
	pushl _Assign1_y
	pushl $4
	popl %eax
	popl %ebx
	addl %ebx, %eax
	pushl %eax
	call writeint
	addl $4, %esp
	pushl $9
	pushl $5
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	call negation
	addl $4, %esp
	pushl %eax
	call writeint
	addl $4, %esp
	pushl $5
	pushl $2
	pushl $3
	call multiply
	addl $8, %esp
	pushl %eax
	popl %ebx
	popl %eax
	subl %ebx, %eax
	pushl %eax
	pushl $1
	popl %eax
	popl %ebx
	addl %ebx, %eax
	pushl %eax
	call writeint
	addl $4, %esp
	ret
.text
.global main
main:
	call _Assign1_start
	pushl $0
	call exit
