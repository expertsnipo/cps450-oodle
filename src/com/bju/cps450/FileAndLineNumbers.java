/* FileAndLineNumbers.java
 * Author: Ethan McGee
 * Date: 2014-01-23
 * 
 * Purpose: Keeps track of all files and their line counts so that given a 
 *          line number from the merged file, it can produce the original
 *          filename and line number
 */
package com.bju.cps450;

import java.util.ArrayList;

public class FileAndLineNumbers {
	private ArrayList<String> files = new ArrayList<String>();
	private ArrayList<Integer> lines = new ArrayList<Integer>();
	private ArrayList<Integer> classes = new ArrayList<Integer>();
	
	public FileAndLineNumbers() {
		
	}
	
	/* addFile
	 * Arguments:
	 * 
	 * Purpose: store a file and its line count
	 */
	public void addFile(String filename, int lineCount) {
		files.add(filename);
		lines.add(lineCount);
		classes.add(0); //helper for figuring out if there's more than one class per source file
	}
	
	/* getFile
	 * Arguments:
	 *   @line - the line number in the merged file
	 * Purpose: returns the original file containing the requested line
	 */
	public String getFile(int line) {
		int i = 0;
		while(i + 1 < files.size() && line > lines.get(i)) {
			line -= lines.get(i);
			++i;
		}
		return files.get(i);
	}
	public String getTruncFile(int line) {
		int i = 0;
		while(i + 1 < files.size() && line > lines.get(i)) {
			line -= lines.get(i);
			++i;
		}
		String result = files.get(i).substring(0, files.get(i).length() - 4);
		return result;
	}
	
	/* getLine
	 * Arguments:
	 *   @line - the line number in the merged file
	 * Purpose: returns the original line number
	 */
	public int getLine(int line) {
		int i = 0;
		while(i + 1 < files.size() && line > lines.get(i)) {
			line -= lines.get(i);
			++i;
		}
		return line;
	}
	
	public void incrementClasses(int line) {
		int i = 0;
		while(i + 1 < files.size() && line > lines.get(i)) {
			line -= lines.get(i);
			++i;
		}
		//classes.get(i);
		int ii = classes.get(i) + 1;
		classes.set(i, ii);
	}
	
	public int getNumClasses(int line) {
		int i = 0;
		while(i + 1 < files.size() && line > lines.get(i)) {
			line -= lines.get(i);
			++i;
		}
		return classes.get(i);
		
	}
	
}
