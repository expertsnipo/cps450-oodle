package com.bju.cps450.declarations;

import com.bju.cps450.types.Type;

public class VariableDeclaration extends Declaration {
	private Integer offset;
	private Boolean isClassVariable;
	private Boolean isParameter;
	//int scope;
	//public static int CLASSVAR = 0, LOCALVAR = 1, GLOBAL = 2, PARAM = 3;
	
		
	public VariableDeclaration(String name, Type t) {
		super(name, t);
		this.offset = -1;
		this.isClassVariable = false;
		this.isParameter = false;
		//this.scope = scope;
	}	
	/*public void setScope(int i) {
		this.scope = i;
	}*/
	public void setOffset(int i) {
		offset = i;
	}
	public int getOffset() {
		return offset;
	}
	public boolean getIsClassVar() {
		return isClassVariable;
	}

	public void setIsClassVar(boolean isClassVariable) {
		this.isClassVariable = isClassVariable;
	}

	public boolean isParameter() {
		return isParameter;
	}

	public void setIsParameter(boolean isParameter) {
		this.isParameter = isParameter;
	}
}
