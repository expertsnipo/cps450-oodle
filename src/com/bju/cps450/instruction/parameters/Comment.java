package com.bju.cps450.instruction.parameters;

public class Comment extends Parameter {
	
	private String comment;
	
	public Comment(String type, String name, int linenum) {
		this.comment = type + ": " + name + " on line " + linenum;
	}
	
	/* toString
	 * Arguments:
	 *   
	 * returns the integer literal as a formatted string
	 */
	public String toString() {
		return this.comment;
	}

}
