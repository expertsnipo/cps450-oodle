/* Instruction.java
 * Author: Ethan McGee
 * Date: 2014-03-13
 * Purpose: wraps assembly commands
 */
package com.bju.cps450.instruction;

public class Instruction {
	public static Instruction add = new Instruction("addl"),
			                  sub = new Instruction("subl"),
			                  push = new Instruction("pushl"),
			                  pop = new Instruction("popl"),
			                  call = new Instruction("call"),
							  ret = new Instruction("ret"),
							  mul = new Instruction("imull"),
							  comment = new Instruction("#"),
							  cmp = new Instruction("cmpl"),
							  jne = new Instruction("jne"),
							  je = new Instruction("je"),
							  jmp = new Instruction("jmp"),
							  move = new Instruction("movl"); 
	
	private String name;
	
	public Instruction(String name) {
		this.name = name;
	}
	
	/* getName
	 * Arguments:
	 *   
	 * Purpose: returns the name of an instruction
	 */
	public String getName() {
		return this.name;
	}
	
}
