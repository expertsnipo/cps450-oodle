/* OodleCodeGenerator.java
 * Author: Kyle Gutschow
 * Date: 2014-03-25
 * Purpose: code generator for Oodle 
 */

package com.bju.cps450;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.bju.cps450.analysis.DepthFirstAdapter;
import com.bju.cps450.node.AAddExpression;
import com.bju.cps450.node.AAndExpression;
import com.bju.cps450.node.AArrayExpression;
import com.bju.cps450.node.AAssignStatementStatement;
import com.bju.cps450.node.ACallExpression;
import com.bju.cps450.node.ACallStatementStatement;
import com.bju.cps450.node.AClassDefinition;
import com.bju.cps450.node.AConcatExpression;
import com.bju.cps450.node.ADivideExpression;
import com.bju.cps450.node.AEqualsExpression;
import com.bju.cps450.node.AFalseExpression;
import com.bju.cps450.node.AGreaterExpression;
import com.bju.cps450.node.AGtEqualExpression;
import com.bju.cps450.node.AIdentifierExpression;
import com.bju.cps450.node.AIfStatementStatement;
import com.bju.cps450.node.AInheritsClass;
import com.bju.cps450.node.AInitializer;
import com.bju.cps450.node.AIntegerExpression;
import com.bju.cps450.node.ALoopStatementStatement;
import com.bju.cps450.node.AMeExpression;
import com.bju.cps450.node.AMethod;
import com.bju.cps450.node.AMultExpression;
import com.bju.cps450.node.ANegExpression;
import com.bju.cps450.node.ANewExpression;
import com.bju.cps450.node.ANotExpression;
import com.bju.cps450.node.ANullExpression;
import com.bju.cps450.node.AOrExpression;
import com.bju.cps450.node.AParenExpression;
import com.bju.cps450.node.APosExpression;
import com.bju.cps450.node.AStringExpression;
import com.bju.cps450.node.ASubtractExpression;
import com.bju.cps450.node.ATrueExpression;
import com.bju.cps450.node.AVariable;
import com.bju.cps450.node.Node;
import com.bju.cps450.declarations.ClassDeclaration;
import com.bju.cps450.declarations.MethodDeclaration;
import com.bju.cps450.declarations.VariableDeclaration;
import com.bju.cps450.instruction.Instruction;
import com.bju.cps450.instruction.InstructionCommand;
import com.bju.cps450.instruction.InstructionSet;
import com.bju.cps450.instruction.LabelInstruction;
import com.bju.cps450.instruction.StabsInstruction;
import com.bju.cps450.instruction.parameters.Comment;
import com.bju.cps450.instruction.parameters.IntegerLiteral;
import com.bju.cps450.instruction.parameters.Label;
import com.bju.cps450.instruction.parameters.OffsetRegister;
import com.bju.cps450.instruction.parameters.Register;

public class OodleCodeGenerator extends DepthFirstAdapter {
	
	private HashMap<Node, HashMap<String, Object>> attributeGrammarMap = new HashMap<Node, HashMap<String, Object>>();
	private ArrayList<InstructionSet> classAssembly = new ArrayList<InstructionSet>();
	private ClassDeclaration currentClass;
	private MethodDeclaration currentMethod;
	private String startMethod;
	private int ifstmts = 0, eqstmts = 0, loopstmts = 0;
	

	/* initNode
	 * Arguments:
	 *   node : Node - the node to init
	 * Purpose: helper to prevent null point exceptions
	 */
	private void initNode(Node node) {
		if(attributeGrammarMap.get(node) == null) {
			attributeGrammarMap.put(node, new HashMap<String, Object>());
		}
	}
	
	/* writeAssembly
	 * Arguments:
	 *   
	 * Purpose: generates a temporary assembly file containing the assembly code for the classes
	 */
	public void writeAssembly() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(Application.getFileAndLineNumbers().getTruncFile(0) + ".s")); //assembly file needs the same name as source file
			writer.write("STDOUT = 1\n");
			writer.write("STDIN = 0\n");
			for(InstructionSet assm : classAssembly) {
				writer.write(assm.toAssembly());
			}
			writer.write(".text\n");
			writer.write(".global main\n");
			writer.write("main:\n");
			writer.write("\tcall " + startMethod + "\n");
			writer.write("\tpushl $0\n");
			writer.write("\tcall exit\n");
			writer.close();
		} catch (IOException e) {
			System.out.println("could not generate assembly due to " + e.getMessage());
		}	
	}

	@Override
	public void inAClassDefinition(AClassDefinition node) {
		super.inAClassDefinition(node);
		try {
			currentClass = (ClassDeclaration)Application.getSymbolTable().lookup(node.getFirst().getText(), 0).getDeclaration();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	@Override
	public void outAClassDefinition(AClassDefinition node) {
		super.outAClassDefinition(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		instructions.appendInstruction(new StabsInstruction("data"));
		for(int i = 0; i < node.getVariable().size(); ++i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getVariable().get(i)).get("code"));
		}
		for(int i = 0; i < node.getMethod().size(); ++i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getMethod().get(i)).get("code"));
		}
		classAssembly.add(instructions);
	}

	@Override
	public void inAMethod(AMethod node) {
		super.inAMethod(node);
		try {
			currentMethod = currentClass.lookupMethod(node.getFirst().getText());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void outAInheritsClass(AInheritsClass node) {
		// TODO phase 5
		super.outAInheritsClass(node);
	}

	@Override
	public void outAVariable(AVariable node) {
		super.outAVariable(node);
		initNode(node);
		try {
			initNode(node);
			InstructionSet instructions = new InstructionSet();
			VariableDeclaration var = null;
			if(currentMethod == null) {
				var = currentClass.lookupVariable(node.getIdentifier().getText());
			} else {
				var = currentMethod.lookupVariable(node.getIdentifier().getText());
			}
			// comment assembly
			//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Variable", node.getIdentifier().getText(),node.getIdentifier().getLine())));
			instructions.appendInstruction(new StabsInstruction("comm", var.getAssemblyName() + ", 4, 4"));
			attributeGrammarMap.get(node).put("code", instructions);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}	
		
	}

	@Override
	public void outAMethod(AMethod node) {
		super.outAMethod(node);
		initNode(node);
		

		if(currentMethod.getName().equals("start")) {
			startMethod = currentMethod.getAssemblyName();
		}

		InstructionSet instructions = new InstructionSet();
		instructions.appendInstruction(new StabsInstruction("text"));
		instructions.appendInstruction(new LabelInstruction(currentMethod.getAssemblyName()));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.ebp));
		instructions.appendInstruction(new InstructionCommand(Instruction.move, Register.esp, Register.ebp));
		for (int i = 0; i < currentMethod.getVariables().size(); ++i) {
			instructions.appendInstruction(new InstructionCommand(Instruction.push, new IntegerLiteral(0)));
		}		
		for(int i = 0; i < node.getStatement().size(); ++i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getStatement().get(i)).get("code"));
		}
		VariableDeclaration var = null;
		try {
			var = currentMethod.lookupVariable(currentMethod.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		instructions.appendInstruction(new InstructionCommand(Instruction.move, new OffsetRegister(Register.ebp, ((var.getOffset() +1) * -4)), Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.move, Register.ebp, Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.ebp));
		instructions.appendInstruction(new InstructionCommand(Instruction.ret));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAIfStatementStatement(AIfStatementStatement node) {
		super.outAIfStatementStatement(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("If Statement", node.getExpression().toString(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.cmp, new IntegerLiteral(Integer.parseInt("0")), Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.jne, new Label("_if" + (++ifstmts))));
		instructions.appendInstruction(new InstructionCommand(Instruction.jmp, new Label("_else" + (ifstmts))));
		instructions.appendInstruction(new LabelInstruction("_if" + ifstmts));
		for(int i = 0; i < node.getIf().size(); ++i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getIf().get(i)).get("code"));
		}		
		instructions.appendInstruction(new InstructionCommand(Instruction.jmp, new Label("_endif" + (ifstmts))));
		instructions.appendInstruction(new LabelInstruction("_else" + ifstmts));
		for(int i = 0; i < node.getElse().size(); ++i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getElse().get(i)).get("code"));
		}
		instructions.appendInstruction(new InstructionCommand(Instruction.jmp, new Label("_endif" + (ifstmts))));
		instructions.appendInstruction(new LabelInstruction("_endif" + ifstmts));
		
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outALoopStatementStatement(ALoopStatementStatement node) {
		super.outALoopStatementStatement(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		instructions.appendInstruction(new LabelInstruction("_while" + (++loopstmts)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression()).get("code"));		
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.cmp, new IntegerLiteral(Integer.parseInt("0")), Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.jne, new Label("_startwhile" + (loopstmts))));
		instructions.appendInstruction(new InstructionCommand(Instruction.jmp, new Label("_endwhile" + (loopstmts))));
		instructions.appendInstruction(new LabelInstruction("_startwhile" + loopstmts));
		for(int i = 0; i < node.getStatement().size(); ++i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getStatement().get(i)).get("code"));
		}		
		instructions.appendInstruction(new InstructionCommand(Instruction.jmp, new Label("_while" + (loopstmts))));
		instructions.appendInstruction(new LabelInstruction("_endwhile" + loopstmts));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAAssignStatementStatement(AAssignStatementStatement node) {
		super.outAAssignStatementStatement(node);
		initNode(node);
		try {
			VariableDeclaration var = currentMethod.lookupVariable(node.getIdentifier().getText());
			InstructionSet instructions = new InstructionSet();
			// comment assembly
			//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Assignment", node.getIdentifier().getText(),node.getIdentifier().getLine())));
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression()).get("code"));
			instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.eax));
			if(var.isParameter()) {				
				instructions.appendInstruction(new InstructionCommand(Instruction.move, Register.eax, new OffsetRegister(Register.ebp, 4 * (2 + var.getOffset()))));
			} else if(!var.getIsClassVar()) {
				instructions.appendInstruction(new InstructionCommand(Instruction.move, Register.eax, new OffsetRegister(Register.ebp, -4 * (1 + var.getOffset()))));
			} else {
				instructions.appendInstruction(new InstructionCommand(Instruction.move, Register.eax, new Label(var.getAssemblyName())));
			}			
			attributeGrammarMap.get(node).put("code", instructions);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public void outACallExpression(ACallExpression node) {
		super.outACallExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("CallExpression", node.getIdentifier().getText(),node.getIdentifier().getLine())));
		for(int i = node.getExpression().size() - 1; i >= 0; --i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression().get(i)).get("code"));
		}
		//InstructionSet dotAssembly = (InstructionSet)attributeGrammarMap.get(node.getDot()).get("code");
		InstructionSet dotAssembly = new InstructionSet();
		if (node.getDot() != null) {
			dotAssembly = (InstructionSet)attributeGrammarMap.get(node.getDot()).get("code");
		}
		if(dotAssembly.getCommands().size() == 0 && (node.getIdentifier().getText().equals("readint") || node.getIdentifier().getText().equals("writeint"))) {
			if(node.getIdentifier().getText().equals("readint")) {
				instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("readint")));
				instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
			} else {
				instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("writeint")));
				instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("4")), Register.eax));
			}
		} else {
			try {
				MethodDeclaration method = currentClass.lookupMethod(node.getIdentifier().getText());
				instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label(method.getAssemblyName())));
				instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(node.getExpression().size() * 4), Register.esp));
				instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
			} catch (Exception ex) { ; }
		}
		attributeGrammarMap.get(node).put("code", instructions);
		
	}

	@Override
	public void outACallStatementStatement(ACallStatementStatement node) {
		super.outACallStatementStatement(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("CallStatement", node.getIdentifier().getText(),node.getIdentifier().getLine())));
		for(int i = node.getExpression().size() - 1; i >= 0; --i) {
			instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression().get(i)).get("code"));
		}
		InstructionSet dotAssembly = new InstructionSet();
		if (node.getDot() != null) {
			dotAssembly = (InstructionSet)attributeGrammarMap.get(node.getDot()).get("code");
		}
		if(dotAssembly.getCommands().size() == 0 && (node.getIdentifier().getText().equals("readint") || node.getIdentifier().getText().equals("writeint"))) {
			if(node.getIdentifier().getText().equals("readint")) {
				instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("readint")));
			} else {
				instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("writeint")));
				instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("4")), Register.esp));
			}
		} else {
			try {
				MethodDeclaration method = currentClass.lookupMethod(node.getIdentifier().getText());
				instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label(method.getAssemblyName())));
				instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(node.getExpression().size() * 4), Register.esp));
			} catch (Exception ex) { ; }
		}
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAOrExpression(AOrExpression node) {
		super.outAOrExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Binary Or", node.getLhs() + "or" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("binaryOr")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("8")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAAndExpression(AAndExpression node) {
		super.outAAndExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Binary And", node.getLhs() + "and" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("binaryAnd")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("8")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAGreaterExpression(AGreaterExpression node) {
		super.outAGreaterExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("GreaterThan Comparison", node.getLhs() + ">" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("greater")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("8")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAGtEqualExpression(AGtEqualExpression node) {
		super.outAGtEqualExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("GTEqualTo Comparison", node.getLhs() + ">=" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("gtequalto")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("8")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outADivideExpression(ADivideExpression node) {
		super.outADivideExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Divide expression", node.getLhs() + "/" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("divide")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("8")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAAddExpression(AAddExpression node) {
		super.outAAddExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Add expression", node.getLhs() + "+" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.ebx));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, Register.ebx, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outASubtractExpression(ASubtractExpression node) {
		super.outASubtractExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Subtract expression", node.getLhs() + "-" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.ebx));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.sub, Register.ebx, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAEqualsExpression(AEqualsExpression node) {
		super.outAEqualsExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Equals Expression", node.getLhs() + "=" + node.getRhs() , -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.eax));
		instructions.appendInstruction(new InstructionCommand(Instruction.pop, Register.ebx));
		instructions.appendInstruction(new InstructionCommand(Instruction.cmp, Register.eax, Register.ebx));
		instructions.appendInstruction(new InstructionCommand(Instruction.jne, new Label("_Feq" + (++eqstmts))));
		instructions.appendInstruction(new LabelInstruction("_Teq" + eqstmts));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, new IntegerLiteral(Integer.parseInt("1"))));
		instructions.appendInstruction(new InstructionCommand(Instruction.jmp, new Label("_Eeq" + (eqstmts))));
		instructions.appendInstruction(new LabelInstruction("_Feq" + eqstmts));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, new IntegerLiteral(Integer.parseInt("0"))));
		instructions.appendInstruction(new LabelInstruction("_Eeq" + eqstmts));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAMultExpression(AMultExpression node) {
		super.outAMultExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Multiply expression", node.getLhs() + "*" + node.getRhs(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getLhs()).get("code"));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getRhs()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("multiply")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("8")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		
		attributeGrammarMap.get(node).put("code", instructions);
	}	

	@Override
	public void outAPosExpression(APosExpression node) {
		super.outAPosExpression(node);
		//nothing to do here?
	}

	@Override
	public void outANegExpression(ANegExpression node) {
		super.outANegExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Negation expression", "-" + node.getExpression().toString(), -1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("negation")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("4")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outANotExpression(ANotExpression node) {		
		super.outANotExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Not expression", "not " + node.getExpression().toString(),-1)));
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression()).get("code"));
		instructions.appendInstruction(new InstructionCommand(Instruction.call, new Label("notExpr")));
		instructions.appendInstruction(new InstructionCommand(Instruction.add, new IntegerLiteral(Integer.parseInt("4")), Register.esp));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, Register.eax));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAFalseExpression(AFalseExpression node) {
		super.outAFalseExpression(node);
		initNode(node);		
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("False expression", node.getFalse().getText(), node.getFalse().getLine())));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, new IntegerLiteral(Integer.parseInt("0"))));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outAArrayExpression(AArrayExpression node) {
		// TODO phase 5
		super.outAArrayExpression(node);
	}

	@Override
	public void outAConcatExpression(AConcatExpression node) {
		// TODO phase 5
		super.outAConcatExpression(node);
	}

	@Override
	public void outAIdentifierExpression(AIdentifierExpression node) {
		super.outAIdentifierExpression(node);		
		try {
			initNode(node);
			InstructionSet instructions = new InstructionSet();
			// comment assembly
			//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Identifier expression", node.getIdentifier().getText(), node.getIdentifier().getLine())));
			if(node.getIdentifier().getText().equals("out") || node.getIdentifier().getText().equals("in")) {
				//no-op
			} else {
				VariableDeclaration var;
				if(currentMethod == null) {
					var = currentClass.lookupVariable(node.getIdentifier().getText());
				} else {
					var = currentMethod.lookupVariable(node.getIdentifier().getText());
				}
				if(var.isParameter()) {
					instructions.appendInstruction(new InstructionCommand(Instruction.push, new OffsetRegister(Register.ebp, 4 * (2 + var.getOffset()))));
				} else if(!var.getIsClassVar()) {
					instructions.appendInstruction(new InstructionCommand(Instruction.push, new OffsetRegister(Register.ebp, -4 * (1 + var.getOffset()))));
				} else {
					instructions.appendInstruction(new InstructionCommand(Instruction.push, new Label(var.getAssemblyName())));
				}				
			}
			attributeGrammarMap.get(node).put("code", instructions);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void outAStringExpression(AStringExpression node) {
		super.outAStringExpression(node);
	}

	@Override
	public void outAIntegerExpression(AIntegerExpression node) {
		super.outAIntegerExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("Integer expression", node.getIntegerLiteral().getText(), node.getIntegerLiteral().getLine())));		
		instructions.appendInstruction(new InstructionCommand(Instruction.push, new IntegerLiteral(Integer.parseInt(node.getIntegerLiteral().getText()))));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outATrueExpression(ATrueExpression node) {
		super.outATrueExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		// comment assembly
		//instructions.appendInstruction(new InstructionCommand(Instruction.comment, new Comment("True expression", node.getTrue().getText(), node.getTrue().getLine())));
		instructions.appendInstruction(new InstructionCommand(Instruction.push, new IntegerLiteral(Integer.parseInt("1"))));
		attributeGrammarMap.get(node).put("code", instructions);
	}

	@Override
	public void outANullExpression(ANullExpression node) {
		// TODO phase 5?
		super.outANullExpression(node);
	}

	@Override
	public void outAMeExpression(AMeExpression node) {
		// TODO phase 5
		super.outAMeExpression(node);
	}

	@Override
	public void outANewExpression(ANewExpression node) {
		// TODO phase 5
		super.outANewExpression(node);
	}

	@Override
	public void outAInitializer(AInitializer node) {
		// TODO phase 5
		super.outAInitializer(node);
	}

	@Override
	public void outAParenExpression(AParenExpression node) {
		super.outAParenExpression(node);
		initNode(node);
		InstructionSet instructions = new InstructionSet();
		instructions.appendInstructionSet((InstructionSet)attributeGrammarMap.get(node.getExpression()).get("code"));
		attributeGrammarMap.get(node).put("code", instructions);
	}
	
	
	
	
	
	
	
	
	
	

}
