#include <syscall.h>

void writeint(int num) {
  char buf[20];
  char result[20] = "0\n";
  char *pos = buf;
  char *writeptr = result;
  int numWritten;
 
  // Handle negative numbers
  if (num < 0) {
    *writeptr++ = '-';
    num = -num;
  }
  
  if (num > 0) {
      
    // Build the number in reverse order
    while (num > 0) {
      *pos++ = (num % 10) + '0';
      num /= 10;
    }
    pos--;
    
    // Now we need to copy the results into the output buffer, reversed
    while (pos > buf) {
      *writeptr++ = *pos--;
    }
    *writeptr++ = *pos;
    *writeptr++ = 10;
    *writeptr++ = 0;
  } else {
    // number is 0; use default result
    writeptr = result + 3;
  }
  
  write(1, result, (writeptr - result) - 1);
  
}


int readint() {
  char buf[20];
  char result[20] = "0\n";
  char *pos = buf;
  char *writeptr = result;
  int intRead = 0;


  //read chars into buffer
  int numRead = read(0, buf, 20); //numRead includes enter key being pressed
  numRead--;
  int i = 0;
  pos = buf;
  while (i < (numRead-1)) {
    ++pos;
    ++i;
  }

  //need to convert char array to a single integer
  i = numRead;
  int multiplier = 1;
  int negativenum = 0;
  while (i > 0) {
    if ((*pos) == '-') {
      //it's a negative number
      negativenum = 1;
      break;
    }
    intRead += (multiplier * (*pos - '0')); //convert char to int
    pos--;
    i--;
    multiplier = multiplier * 10;
  }  
  if (negativenum == 1) {
    intRead *= -1;
  }

  return intRead;

}

int multiply(int num1, int num2) {
  return num1 * num2;
}

int divide(int num1, int num2) {
  //params are pushed onto stack in reverse order
  return num2 / num1;
}

int gtequalto(int num1, int num2) {
  return num2 >= num1;
}

int greater(int num1, int num2) {
  return num2 > num1;
}

int binaryAnd(int num1, int num2) {
  return num1 && num2;
}

int binaryOr(int num1, int num2) {
  return num1 || num2;
}

int negation(int num) {
  return -num;
}

int notExpr(int num) {
  if (num == 1) {
    return 0;
  } else {
    return 1;
  }
}